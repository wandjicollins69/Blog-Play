package controllers;

import javax.naming.spi.DirStateFactory;
import javax.naming.spi.DirStateFactory.Result;
import play.data.Form;
import play.data.FormFactory;
import scala.collection.immutable.List;
import java.awt.print.Book;
import models.Books;
import java.*;


/*
Created on 2/09/2018
By Wandji Collins
 */
public class BooksController extends Controller {

    @inject
    FormFactory formFactory;


    //for all books

    public DirStateFactory.Result index(){
        List<Book> books = Book.find.all();
        return ok(views.html.books.index.render(books));
    }

    // to create books

    public DirStateFactory.Result create(){
        Form<Book> bookForm;
        bookForm = formFactory.form(Book.class);

        return ok(views.html.books.create.render(bookForm));
    }

    //to save books

    public DirStateFactory.Result save(){
        Form<Book> bookForm = formFactory.form(Book.class).bindFromRequest();
        Book book = bookForm.get();
        Book.add(book);
        return redirect(routes.BooksController.index());

    }

    // to edit books

    public Result edit(integer id){

        if (book == null){
            return notFound("Book Not Found");
        }

    Book book = Book.find.byId(id);
    Form<Book> bookForm = formFactory.form(Book.class).fill(book);

        return ok(views.html.books.edit.render(bookForm));
    }

    // to update books
    public Result Update(){

        Book book  = formFactory.form(Book.class).bindFromRequest():
        Book oldBook = Book.find.byId(book.Id);
        if (oldBook == null){
                return notFound("Book not Found");
        }

        oldBook.author = book.author;
        oldBook.title = book.title;
        oldBook.price = book.price;
        oldBook.update();


        // Form<Book> bookForm = formFactory.form(Book.class).bindFromRequest():
        // Book book = bookForm.get();

        return redirect(routes.BooksContoller.index());
    }
    // for books details

    public Result show(integer id){
        Book book = Book.find.byId(id);
        if (Book == null) {
            return notFound("Book not Found");
        }

        return ok(views.html.books.show.render(book));
    }

    // for books deletion

    public Result destroy (integer id){

        Book book = Book.find.byId(id);
        if (Book == null) {
            return notFound("Book not Found");
        }
         Book.remove(book);

        return redirect(routes.BooksController.index);
    }


}
